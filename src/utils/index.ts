const whiteCheckSpaceRegex = /^\s*$/;

export const isStringEmpty = (string: string) => {
    return string.match(whiteCheckSpaceRegex) !== null;
};
