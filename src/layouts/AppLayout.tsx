import type { ReactNode } from "react";
import { useLocation, useNavigate } from "react-router";
// Info: here is bug as default import doesn't work for some reason
import routes from "~/routes/routes";
import { HeartOutlined, Layout, Menu, SearchOutlined, type ItemType } from "~/ui";

const { Content, Sider } = Layout;

const items: ItemType[] = [
    { label: "Explore movies", key: routes.movies.explore, icon: <SearchOutlined /> },
    { label: "Favorite movies", key: routes.movies.favorite, icon: <HeartOutlined /> },
];

type P = {
    content: ReactNode;
};

export function AppLayout({ content }: P) {
    const navigate = useNavigate();
    const { pathname } = useLocation();

    return (
        <Layout className="app-layout">
            <Sider theme="light">
                <Menu
                    className="app-layout__menu"
                    mode="inline"
                    items={items}
                    selectedKeys={[pathname]}
                    onClick={(menuItem) => {
                        if (pathname !== menuItem.key) {
                            navigate(menuItem.key);
                        }
                    }}
                />
            </Sider>
            <Layout>
                <Content className="app-layout__content">{content}</Content>
            </Layout>
        </Layout>
    );
}

export default AppLayout;
