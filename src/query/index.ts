import { QueryClient } from "react-query";
import type { QueryKey } from "react-query";

export const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 0,
            retry: 0,
        },
    },
});

export const generateQueryKey = <T extends QueryKey = QueryKey>(data: T): T => JSON.parse(JSON.stringify(data)) as T;

export * from "./useMutation";
export * from "./handleAxiosError";
export * from "./useQuery";
