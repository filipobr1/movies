import { message } from "antd";
import { type AxiosError, isAxiosError } from "axios";
export const handleAxiosError = <T = unknown>(error?: AxiosError<T> | Error | null, customMessage?: string) => {
    if (error !== null) {
        if (customMessage !== undefined) {
            return message.error(customMessage);
        }

        if (isAxiosError(error)) {
            if (error.response === undefined) {
                return message.error("There was a problem, please try again.");
            }

            if (error.response.status === 403) {
                return message.error("Sorry, you are not authorized to perform this action.");
            }

            if (error.response.status === 404) {
                return message.error("Sorry, resource or target does not exist.");
            }
        }

        return message.error("Sorry, something went wrong.");
    }

    return null;
};
