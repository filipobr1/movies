import type { AxiosError } from "axios";
import { useQuery as useQueryReact } from "react-query";
import type { QueryFunction, QueryKey, UseQueryOptions } from "react-query";

export type ResponseErrorDataObject = {
    error?: string;
    message?: string;
    errors?: [string];
};

export const useQuery = <
    TQueryFunctionData,
    TError = AxiosError<ResponseErrorDataObject>,
    TData = TQueryFunctionData,
    TQueryKey extends QueryKey = QueryKey,
>(
    key: TQueryKey,
    query: QueryFunction<TQueryFunctionData, TQueryKey>,
    options?: Omit<UseQueryOptions<TQueryFunctionData, TError, TData, TQueryKey>, "queryFn" | "queryKey">
) =>
    useQueryReact<TQueryFunctionData, TError, TData, TQueryKey>(key, query, {
        staleTime: 5 * 60 * 1000, // 5 Minutes
        cacheTime: 24 * 60 * 60 * 1000, // 1 Day
        ...options,
    });
