import type { AxiosError } from "axios";
import { useMutation as useMutationReact, type MutationFunction, type UseMutationOptions } from "react-query";
import { handleAxiosError } from "~/query/handleAxiosError";
import type { ResponseErrorDataObject } from "~/query/useQuery";

export const useMutation = <
    TData,
    TVariables,
    TError extends AxiosError<ResponseErrorDataObject> = AxiosError<ResponseErrorDataObject>,
    TContext = unknown,
>(
    query: MutationFunction<TData, TVariables>,
    options?: Omit<UseMutationOptions<TData, TError, TVariables, TContext>, "mutationFn" | "mutationKey">
) =>
    useMutationReact<TData, TError, TVariables, TContext>(query, {
        onError(error) {
            void handleAxiosError(error);
        },
        ...options,
    });
