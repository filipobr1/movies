import { Navigate, Route, Routes } from "react-router";
import { MoviesFavorite, MoviesSearch, MoviesDetail, NotFound } from "~/pages";
import { routes } from "~/routes";

export function AppRouter() {
    return (
        <Routes>
            <Route path={routes.movies.explore} element={<MoviesSearch />} />
            <Route path={routes.movies.detail(":id")} element={<MoviesDetail />} />
            <Route path={routes.movies.favorite} element={<MoviesFavorite />} />

            <Route path="/" element={<Navigate replace={true} to={routes.movies.explore} />} />
            <Route path="*" element={<NotFound />} />
        </Routes>
    );
}
