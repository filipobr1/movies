const routes = {
    index: "/",
    movies: {
        detail: (id: string) => `/detail/${id}`,
        explore: "/explore",
        favorite: "/favorite",
    },
};
export default routes;
