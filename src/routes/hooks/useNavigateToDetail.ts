import { useNavigate } from "react-router";
import { routes } from "~/routes";

export const useNavigateToMovieDetail = () => {
    const navigate = useNavigate();

    return { navigateToMovieDetail: ({ id }: { id: string }) => navigate(routes.movies.detail(id)) };
};
