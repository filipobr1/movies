import { Badge } from "~/ui";

function MovieDetailRatingBadge({ rating, scale = 10 }: { rating?: number; scale?: 10 | 100 }) {
    if (rating === undefined) {
        return null;
    }

    const getBadgeColor = () => {
        const recalculatedRating = scale === 100 ? rating / 10 : rating;
        switch (true) {
            case recalculatedRating < 5:
                return "red";
            case recalculatedRating < 7:
                return "orange";
            case recalculatedRating < 9:
                return "blue";
            default:
                return "blue";
        }
    };

    return <Badge color={getBadgeColor()} count={rating} />;
}

export default MovieDetailRatingBadge;
