import { MovieDetailRatingBadge } from "~/features/MoviesDetail/components";
import type { Movie } from "~/models";
import { Descriptions } from "~/ui";

type P = {
    movieData: Movie;
};

function MoviesDetailDescription({ movieData }: P) {
    const { title, actors, awards, boxOffice, genre, runtime, year, director, language, imdbRating, metaScore, plot } =
        movieData;

    return (
        <Descriptions title={title} column={2}>
            <Descriptions.Item label="Actors">{actors}</Descriptions.Item>
            <Descriptions.Item label="Awards">{awards}</Descriptions.Item>
            <Descriptions.Item label="Income">{boxOffice}</Descriptions.Item>
            <Descriptions.Item label="Genre">{genre}</Descriptions.Item>
            <Descriptions.Item label="Runtime">{runtime}</Descriptions.Item>
            <Descriptions.Item label="Released">{year}</Descriptions.Item>
            <Descriptions.Item label="Director">{director}</Descriptions.Item>
            <Descriptions.Item label="Language">{language}</Descriptions.Item>
            <Descriptions.Item label="Imdb rating">
                <MovieDetailRatingBadge rating={imdbRating} />
            </Descriptions.Item>
            <Descriptions.Item label="Meta score">
                <MovieDetailRatingBadge rating={metaScore} scale={100} />
            </Descriptions.Item>
            <Descriptions.Item label="Plot">{plot}</Descriptions.Item>
        </Descriptions>
    );
}

export default MoviesDetailDescription;
