import type { ErrorStateData, SuccessStateData } from "~/common/services";
import { getDetail } from "~/features/MoviesDetail/services";
import { getMovieData } from "~/models";
import type { Movie } from "~/models";
import { useQuery } from "~/query";

export const useGetDetail = ({ id }: { id: string }) => {
    return useQuery<SuccessStateData<Movie> | ErrorStateData>(["movies", "detail", id], async () => {
        const response = await getDetail({ id });

        if (response.data.Error !== undefined) {
            return { error: response.data.Error };
        }

        return { data: getMovieData(response.data) };
    });
};
