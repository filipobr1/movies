import { type AxiosResponse } from "axios";
import { apiClient } from "~/common/services";
import { apiKey } from "~/config";
import type { MovieDataObject } from "~/models/service";
import { routes } from "~/routes";

type RequestErrorState = {
    Error: string;
};

type RequestSuccessState = MovieDataObject & {
    Error?: never;
};

export const getDetail = async ({ id }: { id: string }) => {
    return apiClient.get<MovieDataObject | RequestErrorState, AxiosResponse<RequestSuccessState | RequestErrorState>>(
        routes.index,
        {
            params: { i: id, apikey: apiKey },
        }
    );
};
