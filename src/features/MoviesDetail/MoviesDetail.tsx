import { MoviesFavoriteButton, MoviesTypeTag } from "~/common/components";
import { MoviesDetailDescription } from "~/features/MoviesDetail/components";
import { useGetDetail } from "~/features/MoviesDetail/queries";
import fallbackImage from "~/static/fallback-image.jpg";
import { Image, Row, Col, Skeleton, Space, Result } from "~/ui";

type P = {
    id: string;
};

function MoviesDetail({ id }: P) {
    const { data: queryData, isLoading } = useGetDetail({ id });

    if (isLoading) {
        return <Skeleton active={true} paragraph={{ rows: 12 }} />;
    }

    if (queryData === undefined) {
        return null;
    }

    if (queryData.error !== undefined) {
        return <Result subTitle={queryData.error} />;
    }

    const { data } = queryData;

    return (
        <Row>
            <>
                <Col span={6}>
                    <Image
                        className="movies-detail__image"
                        height={450}
                        width={310}
                        preview={false}
                        src={data.poster}
                        fallback={fallbackImage}
                    />
                </Col>
                <Col span={16}>
                    <Space direction="vertical">
                        <Space direction="vertical" size={[0, 16]}>
                            <Row>
                                <MoviesTypeTag type={data.type} />
                                <MoviesFavoriteButton movieData={data} />
                            </Row>
                            <MoviesDetailDescription movieData={data} />
                        </Space>
                    </Space>
                </Col>
            </>
        </Row>
    );
}

export default MoviesDetail;
