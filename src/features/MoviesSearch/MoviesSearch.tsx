import { useState } from "react";
import { useSearchParameter } from "~/common/hooks";
import { MoviesSearchResult } from "~/features/MoviesSearch/components";
import { SearchInput, SearchOutlined, Button, Alert } from "~/ui";
import { isStringEmpty } from "~/utils";

type P = {
    navigateToMovieDetail: ({ id }: { id: string }) => void;
};

function MoviesSearch({ navigateToMovieDetail }: P) {
    const { searchValue, onSearch } = useSearchParameter();
    const [searchInputValue, setSearchInputValue] = useState<string | undefined>(searchValue);

    const isSearchInvalid = searchInputValue === undefined || isStringEmpty(searchInputValue);

    return (
        <>
            <SearchInput
                className="movies-search__input"
                // eslint-disable-next-line @typescript-eslint/naming-convention
                onSearch={(value, _event, info) => (info?.source === "clear" ? null : onSearch(value))}
                onChange={(value) => setSearchInputValue(value.target.value)}
                allowClear={true}
                value={searchInputValue}
                enterButton={<Button disabled={isSearchInvalid} type="primary" icon={<SearchOutlined />} />}
                onPressEnter={() => (isSearchInvalid ? undefined : onSearch(searchInputValue))}
                addonBefore="Search for movies"
            />
            {searchValue !== undefined ? (
                <MoviesSearchResult navigateToMovieDetail={navigateToMovieDetail} />
            ) : (
                <Alert
                    className="movies-search-result__info"
                    message="Welcome to localhost movies!"
                    description="Please enter name of the movie or series to see the results."
                    type="info"
                />
            )}
        </>
    );
}

export default MoviesSearch;
