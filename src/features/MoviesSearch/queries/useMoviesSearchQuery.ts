import type { ErrorStateData, SuccessStateData } from "~/common/services";
import { searchMovies } from "~/features/MoviesSearch/services";
import { getMovieBaseData, type MovieBase } from "~/models";
import { useQuery } from "~/query";

export type SearchErrorStateData = ErrorStateData & {
    totalResults?: never;
};

export type SearchSuccessStateData = SuccessStateData<MovieBase[]> & {
    totalResults: number;
};

export const useMoviesSearchQuery = ({ search, page = 1 }: { search?: string; page?: number }) => {
    return useQuery<SearchErrorStateData | SearchSuccessStateData>(
        ["movies", "search", search, page],
        async () => {
            if (search === undefined) {
                return {
                    data: [],
                    totalResults: 0,
                };
            }

            const response = await searchMovies({
                search,
                page: page,
            });
            const { Search, Error, totalResults } = response.data;

            if (Error !== undefined) {
                return { error: response.data.Error };
            }

            return {
                data: Search.map((searchItem) => getMovieBaseData(searchItem)),
                totalResults: parseInt(totalResults, 10),
            };
        },
        { enabled: search !== undefined, keepPreviousData: true }
    );
};
