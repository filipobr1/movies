import { type AxiosResponse } from "axios";
import { apiClient } from "~/common/services";
import { apiKey } from "~/config";
import type { MovieBase } from "~/models";
import type { MovieBaseDataObject } from "~/models/service";
import { routes } from "~/routes";

export type RequestErrorState = {
    Error: string;
    Search?: never;
    totalResults?: never;
};

export type RequestSuccessState<T> = {
    Error?: never;
    Search: T[];
    totalResults: string;
};

export const searchMovies = async ({ search, page }: { search: string; page: number }) => {
    return apiClient.get<MovieBase, AxiosResponse<RequestSuccessState<MovieBaseDataObject> | RequestErrorState>>(
        routes.index,
        {
            params: { s: search, apikey: apiKey, page },
        }
    );
};
