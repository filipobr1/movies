import { MoviesDataCard, MoviesPagination } from "~/common/components";
import { usePaginationParameter, useSearchParameter } from "~/common/hooks";
import { useMoviesSearchQuery } from "~/features/MoviesSearch/queries";
import { Card, Col, List, Result, Row, Skeleton } from "~/ui";

type P = {
    navigateToMovieDetail: ({ id }: { id: string }) => void;
};

export function MoviesSearchResult({ navigateToMovieDetail }: P) {
    const { page } = usePaginationParameter();
    const { searchValue } = useSearchParameter();

    const { data: queryData, isFetching } = useMoviesSearchQuery({
        search: searchValue,
        page,
    });

    const skeletonList = (
        <List
            className="movies-search-result__loading"
            grid={{ gutter: 16, column: 6 }}
            dataSource={Array.from({ length: 10 }).fill(1)}
            renderItem={() => (
                <List.Item>
                    <Card>
                        <Skeleton active={true} paragraph={{ rows: 6 }} />
                    </Card>
                </List.Item>
            )}
        />
    );

    if (queryData === undefined) {
        if (isFetching) {
            return skeletonList;
        }
        return null;
    }

    if (queryData.error !== undefined) {
        return <Result title={queryData.error} />;
    }

    return (
        <>
            <Row gutter={[16, 16]} className="movies-search-result__items">
                {isFetching
                    ? skeletonList
                    : queryData.data.map((movieItem) => (
                          <Col span={4} key={movieItem.imdbId}>
                              <MoviesDataCard movieData={movieItem} navigateToMovieDetail={navigateToMovieDetail} />
                          </Col>
                      ))}
            </Row>
            <MoviesPagination totalResults={queryData.totalResults} />
        </>
    );
}
