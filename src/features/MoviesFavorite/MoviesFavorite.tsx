import { useRecoilValue } from "recoil";
import { MoviesDataCard } from "~/common/components";
import { sortedFavoriteMovies } from "~/store";
import { Col, Empty, Row, Title } from "~/ui";

type P = {
    navigateToMovieDetail: ({ id }: { id: string }) => void;
};
function MoviesFavorite({ navigateToMovieDetail }: P) {
    const favoriteMoviesData = useRecoilValue(sortedFavoriteMovies);

    if (favoriteMoviesData.length === 0) {
        return <Empty description="Add movies to favorites to see them here" />;
    }

    return (
        <>
            <Row>
                <Title className="movies-favorite__title" level={1}>
                    Favorite movies
                </Title>
            </Row>
            <Row gutter={[16, 16]}>
                {favoriteMoviesData.map((movieItem) => (
                    <Col span={4} key={movieItem.imdbId}>
                        <MoviesDataCard movieData={movieItem} navigateToMovieDetail={navigateToMovieDetail} />
                    </Col>
                ))}
            </Row>
        </>
    );
}

export default MoviesFavorite;
