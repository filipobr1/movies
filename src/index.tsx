/* istanbul ignore file */
import "~/index.scss";
import reactDOMClient from "react-dom/client";
import { QueryClientProvider } from "react-query";
import { BrowserRouter } from "react-router-dom";
import { RecoilRoot } from "recoil";
import { queryClient } from "~/query";
import { AppRouter } from "~/routes";

const rootContainer = document.createElement("div");
document.body.appendChild(rootContainer);
const root = reactDOMClient.createRoot(rootContainer);
root.render(
    <QueryClientProvider client={queryClient}>
        <RecoilRoot>
            <BrowserRouter>
                <AppRouter />
            </BrowserRouter>
        </RecoilRoot>
    </QueryClientProvider>
);
