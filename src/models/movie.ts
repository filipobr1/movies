import type { MovieBaseDataObject, MovieDataObject } from "~/models/service";

export type MovieType = "movie" | "series" | "episode";

export type MovieBase = {
    imdbId: string;
    year: string;
    poster: string;
    type: MovieType;
    title: string;
};

export type Movie = MovieBase & {
    actors: string;
    awards: string;
    boxOffice: string;
    genre: string;
    runtime: string;
    imdbRating: number;
    plot: string;
    director: string;
    metaScore: number;
    language: string;
};

export const getMovieData = (data: MovieDataObject): Movie => ({
    actors: data.Actors,
    awards: data.Awards,
    boxOffice: data.BoxOffice,
    genre: data.Genre,
    poster: data.Poster,
    runtime: data.Runtime,
    title: data.Title,
    type: data.Type,
    year: data.Year,
    imdbId: data.imdbID,
    imdbRating: parseFloat(data.imdbRating),
    plot: data.Plot,
    director: data.Director,
    metaScore: parseInt(data.Metascore, 10),
    language: data.Language,
});

export const getMovieBaseData = (data: MovieBaseDataObject): MovieBase => ({
    poster: data.Poster,
    title: data.Title,
    type: data.Type,
    year: data.Year,
    imdbId: data.imdbID,
});
