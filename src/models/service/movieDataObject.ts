export type MovieBaseDataObject = {
    imdbID: string;
    Type: "movie" | "series" | "episode";
    Year: string;
    Title: string;
    Poster: string;
};

export type MovieDataObject = MovieBaseDataObject & {
    Actors: string;
    Awards: string;
    BoxOffice: string;
    Genre: string;
    Runtime: string;
    imdbRating: string;
    Plot: string;
    Director: string;
    Metascore: string;
    Language: string;
};
