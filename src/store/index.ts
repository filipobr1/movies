import { atom } from "recoil";
import type { RecoilState } from "recoil";
import { recoilPersist } from "recoil-persist";
import type { MovieBase } from "~/models";

const { persistAtom } = recoilPersist();

export const favoriteMoviesDataAtom: RecoilState<MovieBase[]> = atom({
    key: "favoriteMoviesData",
    default: [],
    // eslint-disable-next-line @typescript-eslint/naming-convention
    effects_UNSTABLE: [persistAtom],
});

export * from "./selectors";
