import { selector } from "recoil";
import { favoriteMoviesDataAtom } from "~/store/index";

export const sortedFavoriteMovies = selector({
    key: "sortedFavoriteMovies",
    get: ({ get }) => {
        const favoriteMovies = get(favoriteMoviesDataAtom);
        const favoriteMoviesToSort = [...favoriteMovies];

        return favoriteMoviesToSort.sort((a, b) => a.title.localeCompare(b.title));
    },
});
