import { useSearchParams } from "react-router-dom";
import type { Parameters } from "~/types";

export const usePaginationParameter = () => {
    const [searchParams, setSearchParams] = useSearchParams();
    const params: Parameters = Object.fromEntries([...searchParams]);
    const page = typeof params.page === "string" ? parseInt(params.page, 10) : undefined;

    return {
        page,
        onPageChange: (pageNumber: number) => {
            setSearchParams({ ...params, page: pageNumber.toString() });
        },
    };
};
