import { useSearchParams } from "react-router-dom";
import type { Parameters } from "~/types";
import { isStringEmpty } from "~/utils";

export const useSearchParameter = () => {
    const [searchParams, setSearchParams] = useSearchParams();
    const params: Parameters = Object.fromEntries([...searchParams]);

    const { search } = params;

    const isSearchValid = search !== undefined && !isStringEmpty(search);

    return {
        searchValue: isSearchValid ? search : undefined,
        onSearch: (searchString: string) => {
            setSearchParams({ search: searchString.trim(), page: "1" });
        },
    };
};
