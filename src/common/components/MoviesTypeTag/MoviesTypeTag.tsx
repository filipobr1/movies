import type { MovieType } from "~/models";
import { Tag } from "~/ui";

function MoviesTypeTag({ type }: { type: MovieType }) {
    const getTagColor = () => {
        switch (type) {
            case "episode":
                return "red";
            case "movie":
                return "magenta";
            case "series":
                return "cyan";
            default:
                return "cyan";
        }
    };

    return (
        <Tag className="movies-type-tag" color={getTagColor()}>
            {type}
        </Tag>
    );
}

export default MoviesTypeTag;
