import { useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { usePaginationParameter } from "~/common/hooks";
import type { Parameters } from "~/types";
import { Pagination } from "~/ui";

type P = {
    totalResults: number;
};

function MoviesPagination({ totalResults }: P) {
    const { page, onPageChange } = usePaginationParameter();
    const [searchParams, setSearchParams] = useSearchParams();
    const params: Parameters = Object.fromEntries([...searchParams]);

    const { search: searchParam, page: pageParam } = params;

    useEffect(() => {
        if (searchParam !== undefined) {
            if (totalResults < 10) {
                setSearchParams({ search: searchParam });
            }

            if (pageParam === undefined && totalResults > 10) {
                setSearchParams({ search: searchParam, page: "1" });
            }
        }
    }, [pageParam, searchParam, setSearchParams, totalResults]);

    if (page === undefined) {
        return null;
    }

    return (
        <Pagination
            hideOnSinglePage={true}
            showSizeChanger={false}
            current={page}
            total={totalResults}
            onChange={onPageChange}
        />
    );
}

export default MoviesPagination;
