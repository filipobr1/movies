import { useRecoilState } from "recoil";
import type { MovieBase } from "~/models";
import { favoriteMoviesDataAtom } from "~/store";
import { HeartFilled, HeartOutlined, Tooltip } from "~/ui";

function MoviesFavoriteButton({ movieData }: { movieData: MovieBase }) {
    const { imdbId: id } = movieData;
    const [favoriteMoviesData, setFavoriteMoviesData] = useRecoilState(favoriteMoviesDataAtom);

    const isFavorite = favoriteMoviesData.find((favoriteMovie) => id === favoriteMovie.imdbId) !== undefined;

    const removeFromFavorites = () => {
        setFavoriteMoviesData(favoriteMoviesData.filter((favoriteMovie) => favoriteMovie.imdbId !== id));
    };

    const addToFavorites = () => {
        setFavoriteMoviesData([...favoriteMoviesData, movieData]);
    };

    const toggleFavorite = () => {
        return isFavorite ? removeFromFavorites() : addToFavorites();
    };

    return (
        <>
            {isFavorite ? (
                <Tooltip title="Remove from favorites">
                    <HeartFilled
                        className="movie-favorite-button movie-favorite-button__filled"
                        onClick={toggleFavorite}
                    />
                </Tooltip>
            ) : (
                <Tooltip title="Add to favorites">
                    <HeartOutlined
                        className="movie-favorite-button movie-favorite-button__outlined"
                        onClick={toggleFavorite}
                    />
                </Tooltip>
            )}
        </>
    );
}

export default MoviesFavoriteButton;
