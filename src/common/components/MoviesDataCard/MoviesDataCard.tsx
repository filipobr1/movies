import { MoviesFavoriteButton, MoviesTypeTag } from "~/common/components";
import type { MovieBase } from "~/models";
import fallbackImage from "~/static/fallback-image.jpg";
import { Card, Image, InfoCircleOutlined, Meta, Row, Tooltip } from "~/ui";

type P = {
    movieData: MovieBase;
    navigateToMovieDetail: ({ id }: { id: string }) => void;
};

function MoviesDataCard({ movieData, navigateToMovieDetail }: P) {
    const { poster, imdbId, title, type, year } = movieData;

    return (
        <Card
            className="movie-data-card"
            cover={
                <Image
                    className="movie-data-card__image"
                    fallback={fallbackImage}
                    height={170}
                    preview={false}
                    src={poster}
                />
            }
            actions={[
                <MoviesFavoriteButton key="favorite" movieData={movieData} />,
                <Tooltip key="detail" title="Go to detail">
                    <InfoCircleOutlined onClick={() => navigateToMovieDetail({ id: imdbId })} />
                </Tooltip>,
            ]}
        >
            <Meta
                title={title}
                description={
                    <Row justify="space-between">
                        <MoviesTypeTag type={type} />
                        {year}
                    </Row>
                }
            />
        </Card>
    );
}

export default MoviesDataCard;
