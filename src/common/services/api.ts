import axios from "axios";
import { baseUrl } from "~/config";

export type ErrorStateData = {
    error: string;
    data?: never;
};

export type SuccessStateData<T> = {
    error?: never;
    data: T;
};
export const apiClient = axios.create({
    responseType: "json",
    baseURL: baseUrl,
});
