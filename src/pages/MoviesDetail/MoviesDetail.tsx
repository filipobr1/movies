import { useParams } from "react-router-dom";
import { MoviesDetail as MoviesDetailContent } from "~/features";
import { AppLayout } from "~/layouts";

export function MoviesDetail() {
    const { id } = useParams<{ id: string }>();

    return <AppLayout content={id !== undefined ? <MoviesDetailContent id={id} /> : null} />;
}
