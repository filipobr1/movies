import { MoviesFavorite as MoviesFavoriteContent } from "~/features";
import { AppLayout } from "~/layouts";
import { useNavigateToMovieDetail } from "~/routes";

export function MoviesFavorite() {
    const { navigateToMovieDetail } = useNavigateToMovieDetail();

    return <AppLayout content={<MoviesFavoriteContent navigateToMovieDetail={navigateToMovieDetail} />} />;
}
