import { MoviesSearch as MoviesSearchContent } from "~/features";
import { AppLayout } from "~/layouts";
import { useNavigateToMovieDetail } from "~/routes";

export function MoviesSearch() {
    const { navigateToMovieDetail } = useNavigateToMovieDetail();

    return <AppLayout content={<MoviesSearchContent navigateToMovieDetail={navigateToMovieDetail} />} />;
}
