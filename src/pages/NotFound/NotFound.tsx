import { AppLayout } from "~/layouts";
import { Result } from "~/ui";

function NotFound() {
    return (
        <AppLayout
            content={<Result status="404" title="404" subTitle="Sorry, the page you visited does not exist." />}
        />
    );
}

export default NotFound;
